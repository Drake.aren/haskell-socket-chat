module Main where 

import Network.Socket
import System.IO
import Control.Exception
import Control.Concurrent
import Control.Monad (when)
import Control.Monad.Fix (fix)
  
type Message = (Int, String)

main :: IO ()
main = do
  sock <- socket AF_INET Stream 0
  setSocketOption sock ReuseAddr 1
  bind sock (SockAddrInet 4242 iNADDR_ANY)
  listen sock 2
  chan <- newChan 
  _ <- forkIO $ fix $ \loop -> do 
    (_, _) <- readChan chan
    loop
  mainLoop sock chan 0

mainLoop :: Socket -> Chan Message -> Int -> IO ()
mainLoop sock chan messageNumber = do
  connection <- accept sock
  forkIO (runConn connection chan messageNumber)
  mainLoop sock chan $! messageNumber + 1

runConn :: (Socket, SockAddr) -> Chan Message -> Int -> IO ()
runConn (sock, _) chan messageNumber = do
  let broadcast message = writeChan chan (messageNumber, message)
  handler <- socketToHandle sock ReadWriteMode
  hSetBuffering handler NoBuffering

  hPutStrLn handler "Enter nickname: "
  nickname <- fmap init (hGetLine handler)
  broadcast ("--> " ++ nickname ++ " entered chat.")
  hPutStrLn handler ("Wellcome " ++ nickname)

  commLine <- dupChan chan 

  reader <- forkIO $ fix $ \loop -> do 
    (nextMessageNumber, line) <- readChan commLine
    when (messageNumber /= nextMessageNumber) $ hPutStrLn handler line
    loop

  handle (\(SomeException _) -> return ()) $ fix $ \loop -> do
    line <- fmap init (hGetLine handler)
    case line of 
        "exit" -> hPutStrLn handler "exiting..."
        _      -> broadcast (nickname ++ ": " ++ line) >> loop

  killThread reader
  broadcast ("<-- " ++ nickname ++ " exited.")
  hClose handler